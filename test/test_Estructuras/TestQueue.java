package test_Estructuras;

import org.junit.Before;
import junit.framework.TestCase;
import model.data_structures.Queue;

public class TestQueue extends TestCase{

	Queue <Integer> queue;

	@Before
	public void setUp()
	{
		queue= new Queue <Integer>();
		queue.enqueue(1);
		queue.enqueue(5);
		queue.enqueue(3);
	}

	public void testEnqueue()
	{
		queue.enqueue(6);
		assertEquals("No se a�adi� el elemento al final", 6, queue.darElem(4).intValue());
	}

	public void testDequeue()
	{
		assertEquals("No se elimin� el primero",1,queue.dequeue().intValue());
	}

	public void testSizeQ() throws Exception
	{
		assertEquals("El tama�o no es el esperado",3,queue.size());
	}

	public void testEmpty()
	{
		queue.dequeue();
		queue.dequeue();
		queue.dequeue();
		assertTrue("No retorna si la cola est� vac�a", queue.isEmpty());
	}
}
