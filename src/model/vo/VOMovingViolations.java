package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId;
	private String location;
	private int totalPaid;
	private String accidentIndicator;
	private String ticketIssueDate;
	private String violationDesc;
	private String violationCode;


	public VOMovingViolations(int pId, String pLocation, String pTicketIssueDate, int pTotalPaid, String pAccidentIndicator, String pViolationDescription,String pViolationCode)
	{
		objectId=pId;
		location=pLocation;
		ticketIssueDate=pTicketIssueDate;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		violationDesc=pViolationDescription;
		violationCode=pViolationCode;

	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		return objectId;
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		return violationDesc;
	}

	public String getViolationCode()
	{
		return violationCode;
	}
}
