package controller;

import java.io.FileReader;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<VODaylyStatistic> daylyStatisticsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private Stack<VOMovingViolations> movingViolationsStack;

	private VODaylyStatistic estadistica;

	private VOMovingViolations violacion;


	public Controller()
	{
		view = new MovingViolationsManagerView();
		daylyStatisticsQueue=new Queue<VODaylyStatistic>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				this.loadMovingViolations();
				break;

			case 2:
				IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
				view.printDailyStatistics(dailyStatistics);
				break;

			case 3:
				view.printMensage("Ingrese el n�mero de infracciones a buscar");
				int n = sc.nextInt();

				IStack<VOMovingViolations> violations = this.nLastAccidents(n);
				view.printMovingViolations(violations);
				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}



	public void loadMovingViolations()
	{
		try 
		{
			CSVReader reader = new CSVReader(new FileReader("./data/Moving_Violations_Issued_in_January_2018_ordered.csv"));
			String[] nextLine=reader.readNext();

			String dia=null;

			while ((nextLine = reader.readNext()) != null) 
			{				
				//STACK

				violacion=new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[13], Integer.parseInt(nextLine[9]),nextLine[12],nextLine[15],nextLine[14]);
				movingViolationsStack.push(violacion);

				//QUEUE
				boolean infraccion;
				if(nextLine[12].equalsIgnoreCase("No"))
				{
					infraccion=true;
				}
				else 
					infraccion=false;

				if(nextLine[13].split("T")[0].equals(dia))
				{
					estadistica.actualizarEstadistica(Integer.parseInt(nextLine[8]),infraccion);
				}
				else if (!nextLine[13].split("T")[0].equals(dia))
				{
					dia=nextLine[13].split("T")[0];
					estadistica=new VODaylyStatistic(dia);
					estadistica.actualizarEstadistica(Integer.parseInt(nextLine[8]), infraccion);
					daylyStatisticsQueue.enqueue(estadistica);
				}
			}

			reader=new CSVReader(new FileReader("./data/Moving_Violations_Issued_in_February_2018_ordered.csv"));
			nextLine=reader.readNext();
			dia=null;
			while ((nextLine = reader.readNext()) != null) 
			{				
				//STACK
				violacion=new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[13], Integer.parseInt(nextLine[9]),nextLine[12],nextLine[15],nextLine[14]);
				movingViolationsStack.push(violacion);

				//QUEUE
				boolean infraccion;
				if(nextLine[12].equalsIgnoreCase("No"))
				{
					infraccion=true;
				}
				else 
					infraccion=false;

				if(nextLine[13].split("T")[0].equals(dia))
				{
					estadistica.actualizarEstadistica(Integer.parseInt(nextLine[8]),infraccion);
				}
				else if (!nextLine[13].split("T")[0].equals(dia))
				{
					dia=nextLine[13].split("T")[0];
					estadistica=new VODaylyStatistic(dia);
					estadistica.actualizarEstadistica(Integer.parseInt(nextLine[8]), infraccion);
					daylyStatisticsQueue.enqueue(estadistica);
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public Queue <VODaylyStatistic> getDailyStatistics () 
	{
		return daylyStatisticsQueue;
	}

	public Stack <VOMovingViolations> nLastAccidents(int n) 
	{
		Stack<VOMovingViolations> retornar = new Stack<VOMovingViolations>();
		for(int i = 0; i < n; i++)
		{
			VOMovingViolations add = movingViolationsStack.pop();
			retornar.push(add);
		}

		return retornar;
	}
}
